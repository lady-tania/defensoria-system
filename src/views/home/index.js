import React from 'react';
import Servicios from './components/servicios';
import Prevencion from './components/prevencion';
import Autorizacion from './components/autorizacion';
import Encuentranos from './components/encuentranos';
const Home = () => {
    return (
        <div>
            <Servicios />
            <Prevencion />
            <Autorizacion />
            <Encuentranos />
        </div>
    );
}
export default Home;